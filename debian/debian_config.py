import os.path

# Debian defaults
DEBUG = False

STATIC_ROOT = '/var/lib/project-name/collectstatic/'
STATICFILES_DIRS = ['/var/lib/project-name/static',] + STATICFILES_DIRS
TEMPLATE_DIRS = ['/var/lib/project-name/templates',] + TEMPLATE_DIRS
LOCALE_PATHS = ['/var/lib/project-name/locale',] + LOCALE_PATHS
MEDIA_ROOT = '/var/lib/project-name/media/'

ADMINS = (
        # send tracebacks to root
        ('root', 'root@localhost'),
)

if os.path.exists('/var/lib/project-name/secret_key'):
    SECRET_KEY = file('/var/lib/project-name/secret_key').read()

def read_database_configuration():
    '''Extract database configuration from environment variables'''
    for key in os.environ:
        if key.startswith('DATABASE_'):
            prefix, db_key = key.split('_', 1)
            DATABASES['default'][db_key] = os.environ[key]

read_database_configuration()

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'syslog': {
            'format': '%(levelname)s %(name)s.%(funcName)s: %(message)s',
        },
    },
    'handlers': {
        'syslog': {
            'level': 'INFO',
            'address': '/dev/log',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'syslog',
        },
    },
    'loggers': {
        'project_name': {
            'level': 'INFO',
            'handlers': ['syslog', ],
            'propagate': False,
        },
    },
}

CONFIG_FILE='/etc/project-name/settings.py'
if os.path.exists(CONFIG_FILE):
    execfile('/etc/project-name/settings.py')
