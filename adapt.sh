#!/bin/sh

set -e

echo "Give project name (it must match regexp ^[a-z][a-z0-9-]+$ )"
read PROJECT_NAME

if ! echo $PROJECT_NAME | grep -q '^[a-z][a-z0-9-]\+$'; then
    echo "Invalid project name:" $PROJECT_NAME
    exit 1
fi

UPPER_UNDERSCORED=`echo $PROJECT_NAME | tr a-z A-Z | sed 's/-/_/g'`
LOWER_UNDERSCORED=`echo $PROJECT_NAME | sed 's/-/_/g'`
TITLECASE=`echo $PROJECT_NAME | sed 's/-/ /g;s/.*/\L&/; s/[a-z]*/\u&/g'`

echo Project name: $PROJECT_NAME
echo Uppercase underscored: $UPPER_UNDERSCORED
echo Lowercase underscored: $LOWER_UNDERSCORED
echo Titlecase: $TITLECASE

if [ -d .git ]; then
	MV='git mv'
else
	git init
	MV=mv
fi

sed -i \
   -e "s/project_name/$LOWER_UNDERSCORED/g" \
   -e "s/project-name/$PROJECT_NAME/g" \
   -e "s/PROJECT_NAME/A2_$UPPER_UNDERSCORED_/g" \
   -e "s/Project Name/$TITLECASE/g" \
   *.py */*.py */*/*.py project_name/templates/base.html README COPYING MANIFEST.in `find debian -type f`

for FILE in debian/*; do
	if [ -f $FILE ]; then
		NEWNAME=`echo $FILE | sed "s/project-name/$PROJECT_NAME/"`
		if [ "x$FILE" != "x$NEWNAME" ]; then
			$MV $FILE $NEWNAME
		fi
	fi
done
$MV project_name/static/project_name \
   project_name/static/$LOWER_UNDERSCORED
$MV project_name/project_name \
   project_name/$LOWER_UNDERSCORED
$MV project_name/templates/project_name \
   project_name/templates/$LOWER_UNDERSCORED
$MV project_name $LOWER_UNDERSCORED

git rm adapt.sh || true

git add .
git commit -m "Initialize $PROJECT_NAME"
git checkout --orphan debian
git commit -m "Initialize $PROJECT_NAME debian branch" debian
git checkout master
git rm -fr debian
git commit --amend -m "Initialize $PROJECT_NAME"
git tag -a -m '0.1' v0.1
